import * as firebaseui from 'firebaseui'
import firebase from 'firebase'
export default {
  name: 'header-component',
  components: {},
  props: [],
  data () {
    return {
      isLogedin:true
    }
  },
  computed: {

  },
  created(){
    let self = this
    firebase.auth().onAuthStateChanged(function(user) {
      if (user){
        self.isLogedin = true
      }else{
        self.isLogedin = false
      }
    })
  },
  mounted () {  
  },
  methods: {
    /**
     * Logout user 
     * @public
     **/
    singOut(){
        firebase.auth().signOut();
        this.isLogedin = false
     
   }
  }
}
