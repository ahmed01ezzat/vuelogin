import * as firebaseui from 'firebaseui'
import firebase from 'firebase'

export default {
  name: 'home',
  components: {},
  props: [],
  data () {
    return {
      userData: {}
    }
  },
  computed: {

  },
  mounted () {
    if(!this.userData.length){
      this.callback()
    }
  },
  methods: {
    logout(){
      return this.$parent.$children[0].singOut()
    },
    callback(){
      let self = this
      firebase.auth().onAuthStateChanged(function(user) {
        self.userData = user
      })
    }
  }
}
